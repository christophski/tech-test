# Docker image describing a container for our publishing platform.
#
# NginX and uWsgi front the application and are handled by the base docker image.
#
# See the docker-compose file for required/expected environment variables.
#
# Note that the prestart.sh file will be called prior to uWsgi startup

FROM tiangolo/uwsgi-nginx-flask:python3.8-alpine
RUN apk update && apk add postgresql-dev libffi-dev build-base

WORKDIR /app
COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 5000
