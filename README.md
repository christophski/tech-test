# Chris Smith Technical Test Submission

This repository contains code and assets relating to a publishing platform API.

__NOTE:__

The initial project structure and some code was based off the following project:

```
https://github.com/cosmic-byte/flask-restplus-boilerplate.git
```
While nearly everything is new or has changed from the origin, the Makefile, parts of this readme, and a small amount
of code (mostly config/bootstrapping code, etc) may still be recognisable as the orginal author's work.

- [Chris Smith Technical Test Submission](#chris-smith-technical-test-submission)
  - [Developing with Docker (preferred)](#developing-with-docker-preferred)
    - [Get setup](#get-setup)
      - [Running the application in Development mode](#running-the-application-in-development-mode)
      - [Run Tests](#run-tests)
  - [Authenticating with APIs](#authenticating-with-apis)
  - [Examples](#examples)
    - [Player Examples](#player-examples)
      - [Creating a player](#creating-a-player)
      - [Subscribing to a game](#subscribing-to-a-game)
      - [Unsubscribing](#unsubscribing)
    - [Other types of users and entities](#other-types-of-users-and-entities)
    - [Reports](#reports)
  - [Notes](#notes)
    - [Security](#security)
    - [Performance](#performance)
    - [Testing](#testing)
    - [Reporting](#reporting)
    - [Design Improvements](#design-improvements)


## Developing with Docker (preferred)

The following sections provide instructions for developing the platform using docker.

### Get setup

Please ensure you the following installed:

* Docker (tested on 19.03.12)
* Docker compose (v18.02.0+)

#### Running the application in Development mode

Docker compose is used to bring up and run the publishing platform for development:

```
docker-compose up dev-platform
```

This will:

* Bring up a Postgres image as the backend datastore for our platform.
* Bring up the publishing platform container and:
  * Bind mount this repository into the container so that local changes are picked up in the container.
  * Run any migrations (note, probably not suitable for production environments)
  * Run the publishing platform app (available at `localhost:5000`)

Note also that the publishing app is configured to auto-reload when it detects changes in the python source. This
is useful for development but not suitable for production deployments.

#### Run Tests

Tests can also be run in docker:

```
docker-compose run tests
```

## Authenticating with APIs

JSON Web Tokens (JWT) are used to provide stateless authentication throughout the platform.

Registering or logging in will return a token that can be used in subsequent requests.

In order to use the token you must include it in the header of any further requests.

__Note:__ Tokens are set to expire after 10 minutes - when a token expire, you will need to login again.

## Examples

Firstly, follow section on getting setup, and make sure the service is running:

```
docker-compose up dev-platform
```

### Player Examples

#### Creating a player

The first thing to do is register ourselves a player:

```
curl -X POST "http://127.0.0.1:5000/player/" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"username\": \"chris\", \"password\": \"secret_password\", \"email\": \"chris@chris.com\"}"
```

The response body will contain the following:

```
{
  "status": "success",
  "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2NsYWltcyI6eyJyb2xlIjoicGxheWVyIn0sImp0aSI6IjZiNjMwMTg4LTYwYzQtNGU1Ni1hMThlLWRiZGYzMTU2Y2NmYyIsImV4cCI6MTU5Nzc3MTYzNCwiZnJlc2giOmZhbHNlLCJpYXQiOjE1OTc3NzA3MzQsInR5cGUiOiJhY2Nlc3MiLCJuYmYiOjE1OTc3NzA3MzQsImlkZW50aXR5IjoxfQ.rcxZ0tMhWXWNMEgSI_LEQNEA1ba-Id9QqjvsVApcJNQ"
}
```

Note the `Authorization` token that is returned to us. We must include this in any subsequent requests.


#### Subscribing to a game

Now we are registered, we can subscribe to a game.

I've heard good things about `s1_g1` and happen to know it has an id of `1` - let's subscribe to that:

```
curl -X POST "http://127.0.0.1:5000/player/sub/1" -H "accept: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2NsYWltcyI6eyJyb2xlIjoicGxheWVyIn0sImp0aSI6IjZiNjMwMTg4LTYwYzQtNGU1Ni1hMThlLWRiZGYzMTU2Y2NmYyIsImV4cCI6MTU5Nzc3MTYzNCwiZnJlc2giOmZhbHNlLCJpYXQiOjE1OTc3NzA3MzQsInR5cGUiOiJhY2Nlc3MiLCJuYmYiOjE1OTc3NzA3MzQsImlkZW50aXR5IjoxfQ.rcxZ0tMhWXWNMEgSI_LEQNEA1ba-Id9QqjvsVApcJNQ"
```

I should get the following back:


```
{
    "status": "success",
    "msg": "Title added"
}
```

If I try and subscribe to the same game more than once, I'll get the following:

```
{
    "status": "fail",
    "msg": "Already subscribed"
}
```

#### Unsubscribing

Turns out the hype about `s1_g1` was...just hype.

Time to unsubscribe:


```
curl -X DELETE "http://127.0.0.1:5000/player/sub/1" -H "accept: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2NsYWltcyI6eyJyb2xlIjoicGxheWVyIn0sImp0aSI6IjZiNjMwMTg4LTYwYzQtNGU1Ni1hMThlLWRiZGYzMTU2Y2NmYyIsImV4cCI6MTU5Nzc3MTYzNCwiZnJlc2giOmZhbHNlLCJpYXQiOjE1OTc3NzA3MzQsInR5cGUiOiJhY2Nlc3MiLCJuYmYiOjE1OTc3NzA3MzQsImlkZW50aXR5IjoxfQ.rcxZ0tMhWXWNMEgSI_LEQNEA1ba-Id9QqjvsVApcJNQ"
```

Returning:

```
{
    "msg": "Subscription removed",
    "staus": "success"
}
```

### Other types of users and entities

Players subscribe to Titles (games) which are owned by Studios. As there are currently
no APIs for the creation of Studios or Titles, they must be manually created in the database
by the publisher.

To facilitate development and testing, some studios and assosciated titles are created as part
of the Alembic database migrations.

The studios are:

```
-----------------
| id | name     |
-----------------
-----------------
| 1  | studio_1 |
-----------------
| 2  | studio_2 |
-----------------
```

The titles for `studio_1` are:

```
--------------
| id | name  |
--------------
--------------
| 1  | s1_g1 |
--------------
| 2  | s1_g2 |
--------------
```

And the titles for `studio_2` are:

```
--------------
| id | name  |
--------------
--------------
| 3  | s2_g1 |
--------------
```

Aside from players, there are two other actors in the system:

* studio_users - those who have access to the studio management APIs and studio specific reports
* publisher_user - admins who can view backend reports regarding the publishing platform

Currently there are no APIs for these users to register, so to support dev/testing purposes,
a number of these users have been created as part of the database migrations:


```
-------------------------------------------------------------
| email              | password | note                      |
-------------------------------------------------------------
-------------------------------------------------------------
| studio1@test.com   | password | Studio users for studio_1 |
-------------------------------------------------------------
| studio2@test.com   | password | Studio users for studio_2 |
-------------------------------------------------------------
| publisher@test.com | password | Publisher user            |
-------------------------------------------------------------
```

### Reports

There isn't yet a utility for generating test reporting data, however reports are tested.

Currently the following real time reports are implemented:

* studio title popularity reports - show popularity of studio's titles - `/studio/reports/title_popularity`
* publisher reports - show popularity of all titles in platform - `/publisher/reports/title_popularity`

These are tested in:

* `app/test/api/test_publisher_title_popularity_report.py`
* `app/test/api/test_studio_title_popularity_report.py`


## Notes

### Security

* Todo: Currently, passwords are hashed but not salted. Salting is a must for production.
* Todo: All passwords and secrets used are currently dummy values and would need to be replaced with
  strong, protected secrets for production.
* JWT tokens are used to provide stateless API access.

### Performance

* A single application database is shared between services which may prove a bottleneck when scaling.
  A better approach may be to provide different data stores for different services, especially as auth and
  subscription services are likely to have heavy loads and experience spikes.
* Postgresql is used as our application database but we could look to Vitess as a horizontally scalable alternative
* NoSQL data modelling is also an option that would allow horizontal scaling.
* The database and ORM we are using hasn't been tuned in any way and more research/testing would need to be done into
  the use of the ORM to ensure it is performant.

### Testing

* Testing is light and needs expanding.
* Integration testing of APIs dominates the test suite. Refactoring to enable faster, finer grained testing at
  the service level would be better.

### Reporting

* Redis is used to provide efficient, real time reporting of title popularity.
* Todo: Listing users playing on a studio's titles is not yet implemented an 'easy' solution would be to query the application database, although it
  is recognised that this definitely won't scale - some options going forward:
  * An offline data warehouses could be used to provide efficient subscription based querying (although not in real time),
    including which users are subscribed and means for slicing the data to address a range of questions.
    A users-subscribed-to-game report could be exposed through the reporting APIs, or accessed through a BI tool.
  * Subscription events could be used to build a real time table in something like Cassandra - the `Example 2: User Groups`
    example from datastax.com/blog/2015/02/basic-rules-cassandra-data-modelling describes this scenario where the 'group'
    can be the Studio and the data will be clustered on Title.

### Design Improvements

* Models are currently tightly coupled to SQLAlchemy and services are tightly coupled to these models. As a result
  testing of services is mainly performed via e2e tests through api calls. Ideally the models would be decoupled from
  the ORM and repositories would be used to decouple services from underlying storage mechanisms.
* While the app is designed around the concept of services, it may be worth moving to a microservice archiecture
  where different services exist in their own apps and can be developed and deployed independently.
