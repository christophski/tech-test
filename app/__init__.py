from flask_restx import Api
from flask import Blueprint

from app.main.controller.auth_controller import auth_api
from app.main.controller.player_controller import player_api
from app.main.controller.studio_controller import studio_api
from app.main.controller.publisher_controller import publisher_api

api_blueprint = Blueprint('api', __name__)

api = Api(
  api_blueprint,
  title="Publishing Platform",
  version="1.0",
  description="A publishing platform"
)

# Configure our api endpoints
api.add_namespace(auth_api, path="/auth")
api.add_namespace(player_api, path="/player")
api.add_namespace(studio_api, path="/studio")
api.add_namespace(publisher_api, path="/publisher")
