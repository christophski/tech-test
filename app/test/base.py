
from flask_testing import TestCase

from app.main import db, subscription_reporting_service
from manage import app

class BaseTestCase(TestCase):
    """ Base Tests """

    def create_app(self):
        app.config.from_object('app.main.config.TestingConfig')
        return app

    def setUp(self):
        # blat redis
        subscription_reporting_service.rebuild_subscription_report([])
        db.create_all()
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
