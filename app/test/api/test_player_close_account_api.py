import unittest

from app.main import db
import json
from app.main.model.studio import Studio
from app.main.model.title import Title
from app.main.model.player import Player
from app.test.base import BaseTestCase
from app.test.api.request_library import RegisterPlayer
from app.test.api.utils import make_auth_header_from_login_response

class TestPlayerCloseAccountAPI(BaseTestCase):

  def test_closing_player_accounts(self):
    """
    Test that we can delete a player and that
    all our subs are removed and we are no longer able to login
    """

    #def create_title(name, studio)

    def make_player(email, client):
      resp = RegisterPlayer(email=email, username=email).call(client)
      token = make_auth_header_from_login_response(resp)
      user = Player.query.filter_by(email=email).first()

      return token, user

    def make_studio_and_titles(name):
      studio = Studio(name=name)
      title_1 = Title(name="{}_t1".format(name), studio_id=studio.id)
      title_2 = Title(name="{}_t2".format(name), studio_id=studio.id)

      db.session.add(studio)
      db.session.add(title_1)
      db.session.add(title_2)
      db.session.commit()

      return studio, (title_1, title_2)

    with self.client as client:

      # Create some players
      player_1_token, player_1 = make_player("1@web.com", client)
      player_2_token, player_2 = make_player("2@web.com", client)

      # Make some studios with titles
      studio_1, studio_1_titles = make_studio_and_titles("studio_1")
      studio_2, studio_2_titles = make_studio_and_titles("studio_2")

      # generate some subscriptions
      common_title = studio_1_titles[0]
      p1_only_title = studio_2_titles[1]
      p2_only_title = studio_2_titles[0]

      # studio_1, title_1 is common to both players

      player_1.subscriptions.append(common_title)
      player_1.subscriptions.append(p1_only_title)

      player_2.subscriptions.append(common_title)
      player_2.subscriptions.append(p2_only_title)

      db.session.add(player_1)
      db.session.add(player_2)
      db.session.commit()

      # close our p1 account
      close_response = client.delete("/player/", headers=player_1_token)
      self.assertEqual(close_response.status_code, 200)

      # assert that our JWT is no longer valid
      verify_response = client.get("/auth/player", headers=player_1_token)
      self.assertEqual(verify_response.status_code, 200)

      # assert that we haven't affected player 2
      verify_response_2 = client.get("/auth/player", headers=player_2_token)
      self.assertEqual(verify_response_2.status_code, 200)

      # refresh our users from the database
      player_1 = Player.query.filter_by(email="1@web.com").first()
      player_2 = Player.query.filter_by(email="2@web.com").first()

      # make sure we have the correct number of subs for each
      self.assertEquals(len(player_1.subscriptions), 0)
      self.assertEquals(len(player_2.subscriptions), 2)

      # assert that player 1 has a closed_on date
      self.assertTrue(player_1.closed_on is not None)