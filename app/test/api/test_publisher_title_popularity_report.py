import json
import unittest

from app.main import db
from app.main.model.studio import Studio
from app.main.model.title import Title
from app.main.model.player import Player
from app.test.base import BaseTestCase
from app.test.api.request_library import (
  LoginStudioUser, LoginPlayer, LoginPublisherUser, RegisterPlayer, CreateTitle, SubscribePlayerToGame
)
from app.test.api.utils import (
  make_auth_header_from_login_response, parse_response,
  create_studio, create_studio_user, create_publisher_user
)

class TestPublisherTitlePopularityReport(BaseTestCase):

  # note - sorry for the copy paste from test_publisher_title_popularity_report - refactoring required.

  def test_publisher_popularity_report(self):
    """
    Test that the publisher is able to get a popularity report for all titles
    """

    with self.client as client:

      # create publisher user
      create_publisher_user(db, email="pub@test.com")
      pub_token = make_auth_header_from_login_response(LoginPublisherUser(email="pub@test.com").call(client))

      # create a studio and studio user
      studio_1_id = create_studio(db, "studio_1")
      create_studio_user(db, "s1@test.com", studio_1_id)
      studio_1_user_token = make_auth_header_from_login_response(LoginStudioUser(email="s1@test.com").call(client))

      # create a second studio and user
      studio_2_id = create_studio(db, "studio_2")
      create_studio_user(db, "s2@test.com", studio_2_id)
      studio_2_user_token = make_auth_header_from_login_response(LoginStudioUser(email="s2@test.com").call(client))

      # create some titles for studio 1
      CreateTitle(studio_1_user_token, "s1t1").call(client)
      s1t1_id = Title.query.filter(Title.name=="s1t1").first().id

      CreateTitle(studio_1_user_token, "s1t2").call(client)
      s1t2_id = Title.query.filter(Title.name=="s1t2").first().id

      # create some titles for studio 2
      CreateTitle(studio_2_user_token, "s2t1").call(client)
      s2t1_id = Title.query.filter(Title.name=="s2t1").first().id

      CreateTitle(studio_2_user_token, "s2t2").call(client)
      s2t2_id = Title.query.filter(Title.name=="s2t2").first().id

      def make_user(prefix):
        return make_auth_header_from_login_response(RegisterPlayer(username=prefix, email="{}@test.com".format(prefix)).call(client))

      # create some players to subscribe to titles
      p1_token = make_user("p1")
      p2_token = make_user("p2")
      p3_token = make_user("p3")

      # subscribe 2 users to s1t1
      SubscribePlayerToGame(p1_token, s1t1_id).call(client)
      SubscribePlayerToGame(p2_token, s1t1_id).call(client)

      # subscribe 1 user to s1t2
      SubscribePlayerToGame(p1_token, s1t2_id).call(client)

      # subscribe 3 users to s2t1
      SubscribePlayerToGame(p1_token, s2t1_id).call(client)
      SubscribePlayerToGame(p2_token, s2t1_id).call(client)
      SubscribePlayerToGame(p3_token, s2t1_id).call(client)

      # no users subscribe to s2t2

      # The absolute ranking of titles shoudl be:
      # s2t1 -> 3
      # s1t1 -> 2
      # s1t2 -> 1
      # s2t2 -> 0

      publisher_report = parse_response(client.get(
        "/publisher/reports/title_popularity",
        headers=pub_token
      ))

      expected_report = [
        {'title_id': '3', 'studio_id': '2', 'subscriptions': 3, 'absolute_rank': 1, 'studio_rank': 1},
        {'title_id': '1', 'studio_id': '1', 'subscriptions': 2, 'absolute_rank': 2, 'studio_rank': 1},
        {'title_id': '2', 'studio_id': '1', 'subscriptions': 1, 'absolute_rank': 3, 'studio_rank': 2},
        {'title_id': '4', 'studio_id': '2', 'subscriptions': 0, 'absolute_rank': 4, 'studio_rank': 2}
      ]

      self.assertListEqual(publisher_report, expected_report)