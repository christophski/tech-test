import unittest
import json
from datetime import datetime

from app.main import db
from app.main.model.studio import Studio
from app.main.model.studio_user import StudioUser
from app.main.model.publisher_user import PublisherUser

from app.test.api.request_library import (
  RegisterPlayer, LoginPlayer, LoginStudioUser, LoginPublisherUser
)
from app.test.api.utils import (
  create_studio, create_studio_user,
  parse_response, make_auth_header_from_login_response
)
from app.test.base import BaseTestCase

import jwt

class TestAuth(BaseTestCase):

  def _verify_response(self, login_response, expected_claims):
    """
    Helper for testing login api responses
    """

    self.assertEqual(login_response.status_code, 200)
    data = parse_response(login_response)

    self.assertEqual(data["status"], "success")

    token_decoded = jwt.decode(data["Authorization"], verify=False)

    self.assertEqual(token_decoded["user_claims"], expected_claims)
    self.assertEqual(token_decoded["type"], "access")


  def make_publisher_user(self):
    """
    Make a test publisher user
    """

    email = "publisher_user@test.com"
    password = "abc123"

    # create our publisher
    publisher_user = PublisherUser(
      email=email,
      username=email,
      password=password,
      registered_on=datetime.utcnow()
    )
    db.session.add(publisher_user)
    db.session.commit()

    return email, password


  def make_studio_user(self):
    """
    Make a studio user
    """

    email = "studio_user@test.com"
    password = "abc123"

    studio_id = create_studio(db, "studio_a")
    studio_user = create_studio_user(db, email, studio_id, password=password)

    return email, password, studio_id


  def _test_logout_helper(self, resp_with_token, verify_api, client):
    """
    Test helper verifying that users can logout
    """

    token = make_auth_header_from_login_response(resp_with_token)

    # logout
    logout_response = client.delete("/auth/logout", headers=token)
    self.assertEquals(logout_response.status_code, 200)

    # try and verify our token, it should tell us no!
    response = client.get(verify_api, headers=token)
    self.assertEquals(response.status_code, 200)


  def test_player_login(self):
    """
    Test that we can log a player in
    """

    with self.client as client:
      RegisterPlayer().call(client)
      login_response = LoginPlayer().call(client)
      self._verify_response(login_response, {"role": "player"})


  def test_player_logout(self):
    """
    Test we can log a player out and invalidate their token
    """

    with self.client as client:
      RegisterPlayer().call(client)
      login_response = LoginPlayer().call(client)
      self._test_logout_helper(login_response, "/auth/player", client)


  def test_studio_user_login(self):
    """
    Test that we can login a studio user
    """

    email, password, studio_id = self.make_studio_user()

    with self.client as client:
      login_response = LoginStudioUser(email=email, password=password).call(client)
      self._verify_response(login_response, {"role": "studio", "studio_id": studio_id})


  def test_studio_user_logout(self):
    """
    Test we can log a studio_user out and invalidate their token
    """

    email, password, studio_id = self.make_studio_user()

    with self.client as client:
      login_response = LoginStudioUser(email=email, password=password).call(client)
      self._test_logout_helper(login_response, "/auth/studio", client)


  def test_publisher_user_login(self):
    """
    Test that we can login a publisher user
    """

    email, password = self.make_publisher_user()

    with self.client as client:
      login_response = LoginPublisherUser(email=email, password=password).call(client)
      self._verify_response(login_response, {"role": "publisher"})

  def test_publisher_user_logout(self):
    """
    Test we can log a publisher_user out and invalidate their token
    """

    email, password = self.make_publisher_user()

    with self.client as client:
      login_response = LoginPublisherUser(email=email, password=password).call(client)
      self._test_logout_helper(login_response, "/auth/publisher", client)


  def test_player_api_guards(self):
    """
    Todo: put this somewhere better?

    Testing that the 'with_player' api guard/decorator works
    by calling the /login/player api with a get request
    """

    def test(valid, invalids, endpoint, client):
      """
      Test that the token in 'valid' can access the given endpoint
      and that the tokens in invalids can't
      """

      def assert_response_with_token(token_header, status):
        """ """
        response = client.get(
          endpoint,
          headers=token_header
        )
        self.assertEquals(response.status_code, status)

      assert_response_with_token(valid, 200)
      for invalid in invalids:
        assert_response_with_token(invalid, 403)

    # create studio and publisher user
    studio_email, studio_password, studio_id = self.make_studio_user()
    publisher_email, publisher_password = self.make_publisher_user()

    with self.client as client:

      # log our entities in
      player_response = RegisterPlayer().call(client)
      studio_response = LoginStudioUser(email=studio_email, password=studio_password).call(client)
      publisher_response = LoginPublisherUser(email=publisher_email, password=publisher_password).call(client)

      # get their tokens
      player_token = make_auth_header_from_login_response(player_response)
      studio_token = make_auth_header_from_login_response(studio_response)
      publisher_token = make_auth_header_from_login_response(publisher_response)

      # test that the correct users can access the correct endpoint
      test(player_token, (studio_token, publisher_token), "/auth/player", client)
      test(studio_token, (player_token, publisher_token), "/auth/studio", client)
      test(publisher_token, (player_token, studio_token), "/auth/publisher", client)