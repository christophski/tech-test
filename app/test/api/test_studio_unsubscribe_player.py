import json
import unittest

from app.main import db
from app.main.model.studio import Studio
from app.main.model.title import Title
from app.main.model.player import Player
from app.test.base import BaseTestCase
from app.test.api.request_library import LoginStudioUser
from app.test.api.utils import (
  make_auth_header_from_login_response,
  create_player_user,
  create_studio,
  create_studio_user,
  create_title
)

class TestStudioUnsubscribePlayer(BaseTestCase):

  # todo: tests for edge cases:
  #
  #  test_user_is_not_subscribed
  #  test_title_doesnt_exist
  #  test_title_doesnt_belong_to_studio
  #  test_only_studio_users_can_access_api

  def test_studio_can_unsubscribe_subscribed_user_from_title(self):
    """
    Test that a studio is able to unsubscribe a player
    """

    # create a studio and a studio user
    studio_id = create_studio(db, "test")
    create_studio_user(db, "test@test.com", studio_id)

    # create a title for our user to subscribe to
    title = create_title(db, studio_id, "test_title")

    # create a player
    player = create_player_user(db, "test_player@test.com")

    player.subscriptions.append(title)
    db.session.add(player)
    db.session.commit()

    # assert that our player has the subscriptoin
    self.assertEqual(len(Player.query.get(player.id).subscriptions), 1)

    with self.client as client:

      # login our studio user
      resp = LoginStudioUser(email="test@test.com", password="password").call(client)
      token = make_auth_header_from_login_response(resp)

      client.delete(
        "/studio/sub/{}/{}".format(studio_id, player.id),
        headers=token
      )

      # assert that the user is unsubscribed
      self.assertEqual(len(Player.query.get(player.id).subscriptions), 0)
