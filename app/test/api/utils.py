"""
API testing utilities
"""

import json
from datetime import datetime

from app.main.model.studio import Studio
from app.main.model.player import Player
from app.main.model.studio_user import StudioUser
from app.main.model.publisher_user import PublisherUser
from app.main.model.title import Title

def parse_response(response):
  """
  Converts a response from an API call to a Dict.

  Assumes the response is json.
  """

  return json.loads(response.data.decode())


def make_auth_header(token):
  """
  Makes and return a dict with an auth header
  """

  return {"Authorization": "Bearer {}".format(token)}



def make_auth_header_from_login_response(response):
  """
  Return an auth token header that can be included in future responses
  from a login response.
  """

  return make_auth_header(parse_response(response)["Authorization"])

def create_studio(db, name):
  """
  Create a studio and return the id
  """
  studio = Studio(name=name)
  db.session.add(studio)
  db.session.commit()
  return studio.id

def create_publisher_user(db, email, password="password"):
  """
  Create a publisher user
  """
  user = PublisherUser(
    email=email,
    registered_on=datetime.utcnow(),
    username=email,
    password=password
  )

  db.session.add(user)
  db.session.commit()

  return user

def create_studio_user(db, email, studio_id, password="password"):
  """
  Create a studio user for the given studio_id and with the given email.
  """

  user = StudioUser(
    studio_id=studio_id,
    email=email,
    registered_on=datetime.utcnow(),
    username=email,
    password=password
  )

  db.session.add(user)
  db.session.commit()

  return user

def create_player_user(db, email, password="password"):
  """
  Create a player user for the given email and password
  """

  user = Player(
    email=email,
    registered_on=datetime.utcnow(),
    username=email,
    password=password
  )

  db.session.add(user)
  db.session.commit()

  return user

def create_title(db, studio_id, name):
  """
  Create a title with the given studio_id and for the given studio
  """

  title = Title(studio_id=studio_id, name=name)
  db.session.add(title)
  db.session.commit()

  return title
