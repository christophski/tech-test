import unittest

from app.main import db
import json
from app.main.model.studio import Studio
from app.main.model.title import Title
from app.main.model.player import Player
from app.test.base import BaseTestCase
from app.test.api.request_library import RegisterPlayer
from app.test.api.utils import make_auth_header_from_login_response

class TestPlayerSubscriptionAPI(BaseTestCase):

  # todo:
  #
  #   test_error_if_sub_title_doesnt_exist
  #   test_error_if_unsub_title_doesnt_exist
  #   test_user_must_be_authenticated_player

  def test_player_can_sub_and_unsub(self):
    """
    Test that the user can sub to a title
    """

    with self.client as client:

      # create a user
      player_register = RegisterPlayer()
      resp = player_register.call(client)
      token =make_auth_header_from_login_response(resp)

      # create a studio and some titles
      studio = Studio(name="test_studio")
      title_1 = Title(name="test_studio_t1", studio_id=studio.id)
      title_2 = Title(name="test_studio_t2", studio_id=studio.id)

      db.session.add(studio)
      db.session.add(title_1)
      db.session.add(title_2)
      db.session.commit()

      title_2_id = Title.query.filter_by(name="test_studio_t2").first().id

      # call the subscribe api
      sub_result = client.post(
        "/player/sub/{}".format(title_2_id),
        headers=token
      )

      self.assertEqual(sub_result.status_code, 200)

      # get our player and assert the correct sub exists
      player = Player.query.filter_by(email=player_register.email).first()
      self.assertEquals(player.subscriptions[0].id, title_2.id)

      # unsub
      unsub_result = client.delete(
        "/player/sub/{}".format(title_2_id),
        headers=token
      )

      # refresh our player and assert that the sub has gone
      player = Player.query.filter_by(email=player_register.email).first()
      self.assertEquals(len(player.subscriptions), 0)



