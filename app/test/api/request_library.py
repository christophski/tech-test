"""
Reusable API invokations for testing
"""

import json

test_user_name = "chreees"
test_password = "password"
test_email = "chris@work.com"

class RegisterPlayer(object):
  """
  A create user request
  """

  def __init__(self, username=test_user_name, password=test_password, email=test_email):
    self.username = username
    self.password = password
    self.email = email

  def call(self, client):
    """
    Call the api
    """

    return client.post(
      "/player/",
      data=json.dumps(dict(
        email=self.email,
        username=self.username,
        password=self.password
      )),
      content_type="application/json"
    )

class LoginPlayer(object):
  """
  Login a player request
  """

  def __init__(self, password=test_password, email=test_email):
    self.password = password
    self.email = email

  def call(self, client):
    """
    Call the api with the given client
    """

    return client.post(
      "/auth/player",
      data=json.dumps(dict(
        email=self.email,
        password=self.password
      )),
      content_type="application/json"
    )


class LoginStudioUser(object):
  """
  Login a studio user request
  """

  def __init__(self, password=test_password, email=test_email):
    self.password = password
    self.email = email

  def call(self, client):
    """
    Call the api with the given client
    """

    return client.post(
      "/auth/studio",
      data=json.dumps(dict(
        email=self.email,
        password=self.password
      )),
      content_type="application/json"
    )

class CreateTitle(object):
  """
  Create a titile
  """

  def __init__(self, studio_user_auth_token, title_name):
    self.token = studio_user_auth_token
    self.title_name = title_name

  def call(self, client):
    """
    Call the api with the given client
    """

    return client.post(
      "/studio/title",
      data=json.dumps(dict(
        name=self.title_name
      )),
      content_type="application/json",
      headers=self.token
    )


class LoginPublisherUser(object):
  """
  Login a publisher user request
  """

  def __init__(self, password=test_password, email=test_email):
    self.password = password
    self.email = email

  def call(self, client):
    """
    Call the api with the given client
    """

    return client.post(
      "/auth/publisher",
      data=json.dumps(dict(
        email=self.email,
        password=self.password
      )),
      content_type="application/json"
    )


class SubscribePlayerToGame(object):
  """
  Subscribe a player to a game
  """

  def __init__(self, user_token, title_id):
    self.user_token = user_token
    self.title_id = title_id

  def call(self, client):
    """
    Call the api with the given client
    """

    return client.post(
      "/player/sub/{}".format(self.title_id),
      headers=self.user_token
    )
