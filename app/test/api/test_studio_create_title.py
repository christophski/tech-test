import json
import unittest

from app.main import db
from app.main.model.studio import Studio
from app.test.base import BaseTestCase
from app.test.api.request_library import LoginStudioUser
from app.test.api.utils import make_auth_header_from_login_response, create_studio, create_studio_user

class TestStudioCreateTitleAPI(BaseTestCase):

  # todo: tests for edge cases:
  #
  #   test_title_already_exists
  #   test_user_is_not_studio_user

  def test_studio_can_create_title(self):
    """
    Test that the user can create a new title if it doesn't already
    exist
    """

    # create a studio and a studio user
    studio_id = create_studio(db, "test")
    create_studio_user(db, "test@test.com", studio_id)

    with self.client as client:

      # login the studio user
      studio_user_token = make_auth_header_from_login_response(
        LoginStudioUser(email="test@test.com", password="password").call(client)
      )

      # assert no titles for studio
      self.assertEqual(len(Studio.query.get(studio_id).titles), 0)

      # call our api to add a new title
      client.post(
        "/studio/title",
        data=json.dumps(dict(
          name="test_title"
        )),
        content_type="application/json",
        headers=studio_user_token
      )

      # assert that our title was created
      self.assertEqual(Studio.query.get(studio_id).titles[0].name, "test_title")




