import unittest

from app.main import db
import json
from app.test.base import BaseTestCase
from app.test.api.request_library import RegisterPlayer
from app.test.api.utils import parse_response

class TestPlayerRegistrationAPI(BaseTestCase):

  # todo:

  #   test_invalid_input_parameters

  def test_can_register_new_player(self):
    """
    Test that we can create a new player
    """

    with self.client as client:
      response = RegisterPlayer().call(client)
      data = parse_response(response)
      self.assertTrue(data["status"] == "success")
      self.assertTrue(data["Authorization"])
      self.assertTrue(response.content_type == "application/json")
      self.assertEqual(response.status_code, 201)

  def test_cannot_register_same_user_twice(self):
    """
    Ensure we can't create more than one of the same user
    """

    with self.client as client:
      caller = RegisterPlayer()

      # first call should work
      self.assertEqual(caller.call(client).status_code, 201)

      # second call should return error code
      second_call = caller.call(client)
      data = parse_response(second_call)
      self.assertEqual(data["status"], "fail")
      self.assertEqual(data["msg"], "Email in use")
      self.assertEqual(second_call.status_code, 409)


  def test_missing_inputs_in_registration(self):
    """
    Test we get appropriate error messages when data is missing
    """

    with self.client as client:
      result = client.post(
        '/player/',
        data=json.dumps({}),
        content_type="application/json"
      )
      data = parse_response(result)
      self.assertEqual(data["message"], "Input payload validation failed")
      self.assertEqual(data["errors"], {
        "email": "'email' is a required property",
        "username": "'username' is a required property",
        "password": "'password' is a required property"
      })