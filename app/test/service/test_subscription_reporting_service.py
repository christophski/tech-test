from fakeredis import FakeRedis
from unittest import TestCase

from app.main.service.subscription_reporting_service import SubscriptionReportingService

class TestSubscriptionReportingService(TestCase):
  """
  Test our subscription reporting service
  """

  # todo: improve test coverage - light golden path only at the moment

  def _make_sut_with_fake_redis(self):
    """
    Return sut, fake_redis
    """

    fake_redis = FakeRedis()
    sut = SubscriptionReportingService(fake_redis)

    return sut, fake_redis

  def _get_all_keys(self, sut, fake_redis):
    """
    Get all keys from redis for subscription report key
    """

    return fake_redis.zrange(sut._subscription_report_key, 0, 1, withscores=True)


  def test_add_title_to_subscription_report(self):
    """
    Test that we can add title to the subscription report
    """

    sut, fake_redis = self._make_sut_with_fake_redis()

    sut.add_title_to_subscription_report("s1", "t1")

    self.assertEqual(self._get_all_keys(sut, fake_redis), [(b"s1:t1", 0.0)])

  def test_remove_title_from_subscription_report(self):
    """
    Test that we can remove a title from our subscription report
    """

    sut, fake_redis = self._make_sut_with_fake_redis()

    fake_redis.zadd(sut._subscription_report_key, {"s1:t1": 1})

    self.assertEqual(self._get_all_keys(sut, fake_redis), [(b"s1:t1", 1.0)])

    sut.remove_title_from_subscription_report("s1", "t1")

    self.assertEquals(self._get_all_keys(sut, fake_redis), [])

  def test_increase_subscription_count_for_title(self):
    """
    Test that we can increase the subscription count for our title
    """

    sut, fake_redis = self._make_sut_with_fake_redis()

    fake_redis.zadd(sut._subscription_report_key, {"s1:t1": 1})

    self.assertEqual(self._get_all_keys(sut, fake_redis), [(b"s1:t1", 1.0)])

    sut.increase_subscription_count_for_title("s1", "t1")

    self.assertEquals(self._get_all_keys(sut, fake_redis), [(b"s1:t1", 2.0)])

  def test_decrease_subscription_count_for_title(self):
    """
    Test that we can decrease the subscription count for our title
    """

    sut, fake_redis = self._make_sut_with_fake_redis()

    fake_redis.zadd(sut._subscription_report_key, {"s1:t1": 1})

    self.assertEqual(self._get_all_keys(sut, fake_redis), [(b"s1:t1", 1.0)])

    sut.decrease_subscription_count_for_title("s1", "t1")

    self.assertEquals(self._get_all_keys(sut, fake_redis), [(b"s1:t1", 0.0)])

  def test_get_all_titles_ranked_by_subscription(self):
    """
    Test that we can generate a list of titles ranked by subscription
    """

    sut, fake_redis = self._make_sut_with_fake_redis()
    fake_redis.zadd(sut._subscription_report_key, {
      "s1:t1": 100, # abs most subs
      "s1:t2": 50,
      "s1:t3": 10,
      "s2:t4": 90,
      "s2:t5": 99, # abs second most subs
      "s1:t6": 1, # abs least subs
      "s3:t7": 1, # drawn for last place
    })

    def make_ranked_title(sid, tid, subs, abs_rank, stud_rank):
      return dict(
        title_id=tid, studio_id=sid, subscriptions=subs,
        absolute_rank=abs_rank, studio_rank=stud_rank
      )

    expected = [
      make_ranked_title("s1", "t1", 100, 1, 1),
      make_ranked_title("s2", "t5", 99, 2, 1),
      make_ranked_title("s2", "t4", 90, 3, 2),
      make_ranked_title("s1", "t2", 50, 4, 2),
      make_ranked_title("s1", "t3", 10, 5, 3),
      make_ranked_title("s3", "t7", 1, 6, 1),
      make_ranked_title("s1", "t6", 1, 7, 4)
    ]

    report = sut.get_all_titles_ranked_by_subscription()

    self.assertListEqual(report, expected)

  def test_rebuild_subscription_report(self):
    """
    Test that we can re-generate the subscription state in bulk
    """

    sut, fake_redis = self._make_sut_with_fake_redis()
    fake_redis.zadd(sut._subscription_report_key, {
      "s1:t1": 1,
      "s1:t2": 2,
      "s1:t3": 3,
    })

    def make_rebuild_sub(sid, tid, sub_count):
      return dict(
        studio_id=sid, title_id=tid, subs=sub_count
      )

    sut.rebuild_subscription_report((
      make_rebuild_sub("s1", "t1", 100),
      make_rebuild_sub("s2", "t5", 99),
      make_rebuild_sub("s2", "t4", 90),
      make_rebuild_sub("s1", "t2", 50),
      make_rebuild_sub("s1", "t3", 10),
      make_rebuild_sub("s3", "t7", 1),
      make_rebuild_sub("s1", "t6", 1)
    ))

    def make_ranked_title(sid, tid, subs, abs_rank, stud_rank):
      return dict(
        title_id=tid, studio_id=sid, subscriptions=subs,
        absolute_rank=abs_rank, studio_rank=stud_rank
      )

    expected = [
      make_ranked_title("s1", "t1", 100, 1, 1),
      make_ranked_title("s2", "t5", 99, 2, 1),
      make_ranked_title("s2", "t4", 90, 3, 2),
      make_ranked_title("s1", "t2", 50, 4, 2),
      make_ranked_title("s1", "t3", 10, 5, 3),
      make_ranked_title("s3", "t7", 1, 6, 1),
      make_ranked_title("s1", "t6", 1, 7, 4)
    ]

    report = sut.get_all_titles_ranked_by_subscription()

    self.assertListEqual(report, expected)
