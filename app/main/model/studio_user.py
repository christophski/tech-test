
from app.main import db, flask_bcrypt
from app.main.model.user_model_mixin import UserMixin

class StudioUser(db.Model, UserMixin):
  """
  Represents an admin or other user of a studio who can perform things
  like block users and access basic reports.

  Currently, we are not providing more fine grained roles.
  """

  __tablename__ = "studio_user"

  studio_id = db.Column(db.Integer, db.ForeignKey("studio.id"))

