
from app.main import db, flask_bcrypt

# Models the many-to-many relationship between the player and titles they are subscribed to
player_subscription = db.Table(
  "player_subscriptions",
  db.Column("player_id", db.Integer, db.ForeignKey("player.id")),
  db.Column("title_id", db.Integer, db.ForeignKey("title.id"))
)