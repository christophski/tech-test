
from app.main import db

class Studio(db.Model):
  """
  Models a studio (i.e. publisher of games)
  """

  __tablename__ = "studio"

  id = db.Column(db.Integer, primary_key=True, autoincrement=True)
  name = db.Column(db.String(100), nullable=False)
  titles = db.relationship("Title")