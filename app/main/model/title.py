
from app.main import db
from app.main.model.player_subscription_association import player_subscription

class Title(db.Model):
    """
    Represents a title that has been published by a studio
    """

    __tablename__ = "title"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    studio_id = db.Column(db.Integer, db.ForeignKey("studio.id"))

    subscribers = db.relationship(
      "Player",
      secondary=player_subscription,
      back_populates="subscriptions"
    )

    __table_args__ = (db.UniqueConstraint("name", "studio_id"), )

    def __repr__(self):
        return "<Title '{}'>".format(self.name)
