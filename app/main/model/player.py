
from app.main import db, flask_bcrypt
from app.main.model.player_subscription_association import player_subscription
from app.main.model.user_model_mixin import UserMixin

class Player(db.Model, UserMixin):
  """
  Represents a player of games
  """

  __tablename__ = "player"

  subscriptions = db.relationship(
    "Title",
    secondary=player_subscription,
    back_populates="subscribers"
  )

