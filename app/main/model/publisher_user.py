
from app.main import db, flask_bcrypt
from app.main.model.user_model_mixin import UserMixin

class PublisherUser(db.Model, UserMixin):
  """
  Represents an admin or analyst working for the publisher.
  This user will have access to internal reports, etc.

  Currently, we are not providing more fine grained roles (i.e. admin, reporting, custom services, etc)
  """

  __tablename__ = "publisher_user"

