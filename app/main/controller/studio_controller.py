
from flask import request
from flask_restx import Resource, Namespace, fields
from flask_jwt_extended import get_jwt_identity, get_jwt_claims

from app.main.service.auth_service import with_studio
from app.main.service.studio_service import create_title, studio_owns_title
from app.main.service.subscription_service import unsubscribe_from_title
from app.main.service.reporting_service import get_all_ranked_titles_for_studio_report

studio_api = Namespace("studio_api", description="Studio management API")

@studio_api.route("/title")
class TitleManagmenet(Resource):
  """
  Studio title management API
  """

  title_registration_args = studio_api.model(
    "title_registration_args",
    {
      "name": fields.String(required=True, description="The name of the title")
    }
  )

  @studio_api.doc("Create a new title")
  @studio_api.expect(title_registration_args, validate=True)
  @with_studio
  def post(self):
    """
    Create a new title for the studio fo the logged in user
    """

    data = request.json
    studio_id = get_jwt_claims()["studio_id"]

    return create_title(data["name"], studio_id)


@studio_api.route("/sub/<int:title_id>/<int:user_id>")
class TitleSubscription(Resource):
  """
  Studio title subscription management API
  """

  @studio_api.doc("Unsubscribe from a title")
  @with_studio
  def delete(self, title_id, user_id):
    """
    Unsubscribe a user from a title.

    The currently logged in user must belong to the studio that owns the title
    """

    studio_id = get_jwt_claims()["studio_id"]

    if not studio_owns_title(studio_id, title_id):
      return {"status": "fail", "msg": "No title with id {} owned by your studio".format(title_id)}, 404

    return unsubscribe_from_title(user_id, title_id)

@studio_api.route("/reports/title_popularity")
class TitleRankReport(Resource):
  """
  Title popularity report
  """

  @studio_api.doc("Get report showing popularity of your title")
  @with_studio
  def get(self):
    """
    Get the real time ranking report for the users' studio
    """

    studio_id = get_jwt_claims()["studio_id"]

    return get_all_ranked_titles_for_studio_report(studio_id), 200