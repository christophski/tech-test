from flask import request
from flask_restx import Resource, Namespace, fields
from flask_jwt_extended import get_jwt_identity

from app.main.service.auth_service import with_player
from app.main.service.player_service import create_player, close_player_account
from app.main.service.subscription_service import subscribe_to_title, unsubscribe_from_title

player_api = Namespace("player", description="Player related operations")

@player_api.route("/")
class Player(Resource):
  """
  Player management API
  """

  player_registration_args = player_api.model(
    "player_registration_args",
    {
      "email": fields.String(required=True, description="player email"),
      "username": fields.String(required=True, description="player username"),
      "password": fields.String(required=True, description="player password")
    }
  )

  @player_api.expect(player_registration_args, validate=True)
  @player_api.doc("New player signup")
  def post(self):
    """
    Creates a new player, logs them in,
    and returns the Authorization token.
    """

    data = request.json
    res, status = create_player(data["email"], data["username"], data["password"])

    if res.get("Authorization"):
      return res, 201
    else:
      return res, status

  @player_api.doc("Closes the currently authenticated player's account")
  @with_player
  def delete(self):
    """
    Closes the currently authenticated account
    """

    id = get_jwt_identity()
    return close_player_account(id)


@player_api.route("/sub/<int:title_id>")
class PlayerSubscription(Resource):
  """
  Player subscription management API
  """

  @player_api.doc("Subscribe to a title")
  @with_player
  def post(self, title_id):
    """
    Subscribe to a game
    """

    user_id = get_jwt_identity()
    return subscribe_to_title(user_id, title_id)

  @player_api.doc("Unsubscribe from a title")
  @with_player
  def delete(self, title_id):
    """
    Subscribe to a game
    """

    user_id = get_jwt_identity()
    return unsubscribe_from_title(user_id, title_id)
