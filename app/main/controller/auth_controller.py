from flask import request
from flask_restx import Resource, Namespace, fields
from flask_jwt_extended import jwt_required, get_raw_jwt

from app.main.service.auth_service import (
  login_player, login_studio_user, login_publisher_user, logout_user,
  with_publisher, with_player, with_studio
)

# Configure our API
auth_api = Namespace("auth", description="Authentication API")

# Represents the expected inputs for an auth request
user_auth_args = auth_api.model(
  "generic_auth_details",
  {
    "email": fields.String(required=True, description="User email"),
    "password": fields.String(required=True, description="User password")
  }
)

@auth_api.route("/logout")
class UserLogout(Resource):
  """
  Logout methods for any type of user
  """

  @auth_api.doc("Log a user out")
  @jwt_required
  def delete(self):
    logout_user(get_raw_jwt()["jti"])
    return {"msg": "Successfully logged out"}, 200


@auth_api.route("/player")
class PlayerAuth(Resource):
  """
  Player auth methods
  """

  @auth_api.doc("Login a player")
  @auth_api.expect(user_auth_args, validate=True)
  def post(self):
    data = request.json
    return login_player(data["email"], data["password"])

  @auth_api.doc("Verify logged in by token")
  @with_player
  def get(self):
    return {"status": "success", "mgs": "You have a valid token!"}, 200


@auth_api.route("/studio")
class StudioUserAuth(Resource):
  """
  Studio user auth methods
  """

  @auth_api.doc("Login a studio user")
  @auth_api.expect(user_auth_args, validate=True)
  def post(self):
    data = request.json
    return login_studio_user(data["email"], data["password"])

  @auth_api.doc("Verify logged in by token")
  @with_studio
  def get(self):
    return {"status": "success", "mgs": "You have a valid token!"}, 200


@auth_api.route("/publisher")
class PublisherUserAuth(Resource):
  """
  Publisher user auth methods
  """

  @auth_api.doc("Login a publisher")
  @auth_api.expect(user_auth_args, validate=True)
  def post(self):
    data = request.json
    return login_publisher_user(data["email"], data["password"])

  @auth_api.doc("Verify logged in by token")
  @with_publisher
  def get(self):
    return {"status": "success", "mgs": "You have a valid token!"}, 200

