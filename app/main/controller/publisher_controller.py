from flask import request
from flask_restx import Resource, Namespace, fields

from app.main.service.auth_service import with_publisher
from app.main.service.reporting_service import get_all_ranked_titles_report

publisher_api = Namespace("publisher_api", description="Publisher management API")

@publisher_api.route("/reports/title_popularity")
class TitleRankReport(Resource):
  """
  Title popularity report
  """

  @publisher_api.doc("Get report showing popularity of all titles on platform")
  @with_publisher
  def get(self):
    """
    Get the real time ranking report for all titles on the platform
    """

    return get_all_ranked_titles_report(), 200