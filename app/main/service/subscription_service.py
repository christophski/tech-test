import uuid
from datetime import datetime

from app.main import db, subscription_reporting_service
from app.main.model.player import Player
from app.main.model.title import Title

def subscribe_to_title(user_id, title_id):
  """
  Subscribe a player to a title

  todo: currently we don't consider the case that a user has been
  'banned' from a title - i.e. unsubscribed by studio. This isn'togin
  currently modelled.

  todo: Issue subscribe events for historical and real time processing
  """

  player = Player.query.filter_by(id=user_id).first()
  title = Title.query.filter_by(id=title_id).first()

  if title not in player.subscriptions:
    player.subscriptions.append(title)

    db.session.add(player)
    db.session.commit()

    try:
      subscription_reporting_service.increase_subscription_count_for_title(title.studio_id, title.id)
    except Exception as ex:
      # todo: handle this waaay better - I think ideally we would issue an event and then have the
      # subscription reporting service respond to that event which would make it easier to retry.
      print("Unable to increment subscription counter for title")
      print(ex.message)


    return {"status": "success", "msg": "Title added"}, 200
  else:
    return {"status": "fail", "msg": "Already subscribed"}, 200


def unsubscribe_from_title(user_id, title_id):
  """
  Unsubscribe a player from a title

  todo: Issue subscribe events for historical and real time processing
  """

  player = Player.query.filter_by(id=user_id).first()
  title = Title.query.filter_by(id=title_id).first()

  if title not in player.subscriptions:
    return {"status": "fail", "msg": "Not subscribed"}, 200
  else:
    player.subscriptions.remove(title)
    db.session.add(player)
    db.session.commit()

    try:
      subscription_reporting_service.decrease_subscription_count_for_title(title.studio_id, title.id)
    except Exception as ex:
      # todo: handle this waaay better - I think ideally we would issue an event and then have the
      # subscription reporting service respond to that event which would make it easier to retry.
      print("Unable to decrease subscription counter for title")
      print(ex.message)

    return {"staus": "success", "msg": "Subscription removed"}, 200
