
class SubscriptionReportingService():
  """
  Provides methods for managing and accessing the subscription report
  """

  _subscription_report_key = "subscription_report"

  def __init__(self, redis_client):
    """
    Constructor
    """

    self.redis_client = redis_client

  def rebuild_subscription_report(self, subs_list):
    """
    Clears down the real time subscription report and then re-builds it
    from the subs_list.

    subs_list shoudl contain dicts of:

    {
      studio_id: int,
      title_id: int,
      subs: int # the number of current subscriptions
    }

    Useful if the reporting ever falls behind and needs to be updated (i.e. if titles of subs
    are added directly in the database)

    todo: this needs some work - if new subscripotoins come in while this operation is going on
    they will be lost and we will be out of sync.
    """

    with self.redis_client.pipeline() as pipeline:
      pipeline.delete(self._subscription_report_key)

      for sub in subs_list:
        self.add_title_to_subscription_report(
          sub["studio_id"],
          sub["title_id"],
          subscription_count=sub["subs"],
          client=pipeline
        )

      pipeline.execute()

  def _make_subscription_report_key(self, studio_id, title_id):
    """
    Construct a key for an entry in the subscription report
    """

    return "{}:{}".format(studio_id, title_id)

  def _get_studio_id_and_title_id_from_report_key(self, report_key):
    """
    Does the opposite of _make_subscription_report_key
    """

    return report_key.decode("utf-8").split(":")

  def add_title_to_subscription_report(self, studio_id, title_id, subscription_count=0, client=None):
    """
    Adds the given title to our subscription reports.

    Designed to be called when a new subscription is created with a subscription_count of 0.

    However, if it is necessary to insert an existing title with an existing subscription count then a subscription
    count can be provided.
    """

    if client is None:
      client = self.redis_client

    return bool(
      client.zadd(
        self._subscription_report_key,
        { self._make_subscription_report_key(studio_id, title_id): subscription_count }
      )
    )

  def remove_title_from_subscription_report(self, studio_id, title_id):
    """
    Removes the given title from the subscription report.

    This method could be used to 'rollback' a failed title creation.
    """

    return bool(
      self.redis_client.zrem(
        self._subscription_report_key,
        self._make_subscription_report_key(studio_id, title_id)
      )
    )

  def increase_subscription_count_for_title(self, studio_id, title_id):
    """
    Increases the subscription count for the title by 1
    """

    return bool(
      self.redis_client.zincrby(
        self._subscription_report_key,
        1,
        self._make_subscription_report_key(studio_id, title_id)
      )
    )

  def decrease_subscription_count_for_title(self, studio_id, title_id):
    """
    Decreases the subscription count for the title by 1
    """

    return bool(
      self.redis_client.zincrby(
        self._subscription_report_key,
        -1,
        self._make_subscription_report_key(studio_id, title_id)
      )
    )

  def get_all_titles_ranked_by_subscription(self):
    """
    Get all titles as a list, ordered by subscriptions descending. Return values is:

    [
      {
        title_id: int, # the id of the title
        studio_id: int, # the id of the studio that owns the title
        subscriptions: int, # the number of current subscriptions to this title
        absolute_rank: int, # the absolute rank amongst all games on the platform - 1 is top position
        studio_rank: int # the rank of the game amongst all games of the same studio - 1 is top position
      }
    ]

    This is a basic implementation that won't rank equal subs equally (one will win)
    """

    raw_ranked_titles = self.redis_client.zrevrange(self._subscription_report_key, 0, -1, withscores=True)

    ranked_titles = []

    # map to keep track of title rank by studio
    studio_id_rank = { }

    for i, raw_sub in enumerate(raw_ranked_titles):
      studio_id, title_id = self._get_studio_id_and_title_id_from_report_key(raw_sub[0])
      subscriptions = int(raw_sub[1])

      if studio_id not in studio_id_rank:
        studio_id_rank[studio_id] = 0

      studio_rank = studio_id_rank[studio_id]

      ranked_titles.append(dict(
        title_id=title_id,
        studio_id=studio_id,
        subscriptions=subscriptions,
        absolute_rank=i + 1,
        studio_rank=studio_rank + 1
      ))

      studio_id_rank[studio_id] = studio_rank + 1

    return ranked_titles
