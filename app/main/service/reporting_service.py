
from app.main import subscription_reporting_service

from app.main.model.title import Title

def rebuild_subscription_report():
  """
  Rebuilds the real time subscription report
  """

  subs = [
    {
      "studio_id": title.studio_id,
      "title_id": title.id,
      "subs": len(title.subscribers)
    }
    for title in Title.query.all()
  ]

  subscription_reporting_service.rebuild_subscription_report(subs)

def get_all_ranked_titles_report():
  """
  Gets a report for all ranked titles.

  todo: currently we don't get additional informatoin for each title but we may need to
  """

  return subscription_reporting_service.get_all_titles_ranked_by_subscription()

def get_all_ranked_titles_for_studio_report(studio_id):
  """
  Gets a report with all titles belonging to a studio

  todo: currently we don't get additional information for each title but we may need to
  """

  all_titles = subscription_reporting_service.get_all_titles_ranked_by_subscription()

  return [x for x in all_titles if x["studio_id"] == str(studio_id)]