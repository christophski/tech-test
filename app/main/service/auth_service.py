from datetime import timedelta
from functools import wraps

from flask_jwt_extended import (
  create_access_token,
  verify_jwt_in_request,
  get_jwt_claims
)

from app.main import db, redis_client
from app.main.model.player import Player
from app.main.model.publisher_user import PublisherUser
from app.main.model.studio_user import StudioUser


# Define some constants for role types
PUBLISHER_ROLE = "publisher"
PLAYER_ROLE = "player"
STUDIO_ROLE = "studio"

# todo, move this somewhere where it's easier to manipulate during tests
TOKEN_TTL_SECONDS = 10*60

def _make_decorator(required_role):
  """
  Decorator factory producing decorators that permit access for different roles
  """

  def player_role_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if claims['role'] != required_role:
            return {"status": "fail", "msg": "Not Authorized"}, 403
        else:
            return fn(*args, **kwargs)
    return wrapper

  return player_role_required

# API gates that protect APIs and ensure that only certian types of user
# can access resources.
with_publisher = _make_decorator(PUBLISHER_ROLE)
with_player = _make_decorator(PLAYER_ROLE)
with_studio = _make_decorator(STUDIO_ROLE)

def _login_user(user_model, email, password, role, claims_generator=lambda x: {}):
  """
  Login a generic user model derivative

  The 'role' is a string that will be included in the JWT claims and indicates which type of user this is.

  The claims_generator is a function that, when given an instance of user_model, returns a dictionary
  of claims that will be included in the returned JWT.

  Produces descriptive error messages when:

    * The email or password do not match
    * The account is marked as closed

  Returns an auth token that can be used for subsequent calls to protected
  resources.
  """

  try:
    user = user_model.query.filter_by(email=email).first()
    if user and user.check_password(password):
      if user.closed_on is None:
        claims = claims_generator(user)
        claims.update({"role": role})
        access_token = create_access_token(
          identity=user.id,
          user_claims=claims,
          # set the token to expire in 10 minutes - the user must login again after that
          expires_delta=timedelta(seconds=TOKEN_TTL_SECONDS)
        )
        # Note: to keep things simple for now, we are not going to provide
        # users with refresh tokens, they will just have to re-login.
        return {"status": "success", "Authorization": access_token}, 200
      else:
        return {"status": "fail", "msg": "Account is closed"}, 405
    else:
      return {"status": "fail", "msg": "Email or password do not match"}, 401
  except Exception as ex:
    return {"status": "fail", "msg": "Try Again"}, 500


def login_player(email, password):
  """
  Login a player type user
  """

  return _login_user(Player, email, password, "player")


def login_studio_user(email, password):
  """
  Login a studio type user
  """

  return _login_user(
    StudioUser,
    email,
    password,
    "studio",
    claims_generator = lambda su: {"studio_id": su.studio_id}
  )


def login_publisher_user(email, password):
  """
  Login a publisher type user
  """

  return _login_user(PublisherUser, email, password, "publisher")


def logout_user(access_token):
  """
  Logout a user by invalidating the access token
  """

  return blacklist_jti(access_token)


def blacklist_jti(jti):
  """
  Blacklist the given jti if it isn't already
  """

  msg, status = jti_is_blacklisted(jti)

  if status == 404:
    jti_redis_key = "jti:{}".format(jti)
    redis_client.set(jti_redis_key, "")
    redis_client.expire(jti_redis_key, TOKEN_TTL_SECONDS)
    return {"status": "success"}, 200
  else:
    return {"status": "fail", "msg": "JWT already blacklist"}, 302


def jti_is_blacklisted(jti):
  """
  Returns

    {"status": "success"}, 302

  if the jti exists, else

    {"status": "success"}, 404
  """

  if bool(redis_client.exists("jti:{}".format(jti))):
    return {"status": "success"}, 302
  else:
    return {"status": "success"}, 404