import uuid
from datetime import datetime

from app.main import db
from app.main.model.player import Player
from app.main.model.title import Title
from app.main.service.auth_service import login_player


def create_player(email, username, password):
  """
  Creates a new player and logs them in

  Commits the session transaction
  """

  player = Player.query.filter_by(email=email).first()
  if not player:
    new_player = Player(
      email=email,
      username=username,
      password=password,
      registered_on=datetime.utcnow()
    )

    db.session.add(new_player)
    db.session.commit()

    return login_player(email, password)
  else:
    return {"status": "fail", "msg": "Email in use"}, 409

def close_player_account(user_id):
  """
  Closes a player account.

  This involves the following:

    * Setting the 'closed_on' dt on the player
    * Removes a player's subscriptions
  """

  player = Player.query.filter_by(id=user_id).first()

  player.closed_on = datetime.utcnow()
  player.subscriptions = []

  # todo: issue unsubscribe events

  db.session.add(player)
  db.session.commit()

  return {"status": "success", "msg": "Account closed"}, 200

