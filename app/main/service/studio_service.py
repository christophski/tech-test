import uuid
from datetime import datetime

from app.main import db, subscription_reporting_service
from app.main.model.studio import Studio
from app.main.model.title import Title

def studio_owns_title(studio_id, title_id):
  """
  Determines if the given studio owns the given title
  """

  title = Title.query.get(title_id)

  if not title or title.studio_id != studio_id:
    return False

  return True


def create_title(title_name, studio_id):
  """
  Create a new title for the given studio
  """

  # check if a title with the given name already exists
  title = Title.query.filter(Title.studio_id == studio_id).filter(Title.name == title_name).first()

  if title:
    return {"status": "fail", "msg": "Title already exists with that name"}, 400

  new_title = Title(name=title_name, studio_id=studio_id)

  db.session.add(new_title)
  db.session.commit()
  subscription_reporting_service.add_title_to_subscription_report(studio_id, new_title.id)

  return {"status": "sucess", "msg": "Title created"}, 201