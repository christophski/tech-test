import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY", "super-secret")  # Change this!
    SECRET_KEY = os.getenv("SECRET_KEY", "my_precious_secret_key") # Change this
    DEBUG = False
    USE_MOCK_REDIS = True


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, "publishing_platform_dev.db")
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, "publishing_platform_test.db")
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentDockerConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", "")
    REDIS_URL = os.getenv("REDIS_URL", "")
    USE_MOCK_REDIS = False

config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    development_docker=DevelopmentDockerConfig
)

key = Config.SECRET_KEY
