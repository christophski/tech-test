
def configure_manager(jwt_manager):
  """
  Congigures a JWTManager instance to use our jwt_blacklist service for validating
  and blacklisting JWT tokens.
  """

  @jwt_manager.token_in_blacklist_loader
  def check_if_token_in_blacklist(decrypted_token):
    """
    Determine if the token is blacklisted
    """

    from app.main.service.auth_service import jti_is_blacklisted

    msg, status = jti_is_blacklisted(decrypted_token["jti"])

    return status == 301