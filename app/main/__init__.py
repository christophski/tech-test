from flask import Flask
from flask_bcrypt import Bcrypt
from flask_redis import FlaskRedis
from flask_sqlalchemy import SQLAlchemy
from fakeredis import FakeRedis

from app.main.util.jwt import configure_manager
from app.main.service.subscription_reporting_service import SubscriptionReportingService

from flask_jwt_extended import JWTManager

from .config import config_by_name

db = SQLAlchemy()
flask_bcrypt = Bcrypt()
redis_client = FlaskRedis()
subscription_reporting_service = SubscriptionReportingService(redis_client)

def create_app(config_name):
  """
  App factory using provided configuration name
  """

  app = Flask(__name__)
  app.config.from_object(config_by_name[config_name])
  configure_manager(JWTManager(app))
  db.init_app(app)
  flask_bcrypt.init_app(app)

  if app.config.get("USE_MOCK_REDIS"):
    redis_client.provider_class = FakeRedis

  redis_client.init_app(app)

  return app