.ONESHELL:

.PHONY: install setup_db tests run

install:
	python3 -m venv; \
	. venv/bin/activate; \
	pip install -r requirements.txt;

setup_db:
	. venv/bin/activate; \
	python manage.py db upgrade

tests:
	. venv/bin/activate; \
	python manage.py test

run:
	. venv/bin/activate; \
	python manage.py run

