
# Script run during app launch by uwsgi in API docker container.
# This probably wouldn't be suitable for production - i.e. we don't want to run migrations every time the
# app container is launched
python manage.py db upgrade