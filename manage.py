import os
import unittest

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from app import api_blueprint
from app.main import create_app, db
import app.main.model

app = create_app(os.getenv('PUBLISHER_APP_ENV') or 'dev')
app.register_blueprint(api_blueprint)

app.app_context().push()

manager = Manager(app)

migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)

@manager.command
def run():
    app.run()
    # todo: in real life it would not be good to be regenerating the real time reports every time a new
    # instance of this app is created. However for early development it's useful as it makes sure
    # entities loaded in migrations are included in the reports.
    from app.main.service.reporting_service import rebuild_subscription_report
    rebuild_subscription_report()


@manager.command
def test():
    """Runs the unit tests."""
    tests = unittest.TestLoader().discover('app/test', pattern='test_*.py')
    result = unittest.TextTestRunner(verbosity=4).run(tests)
    if result.wasSuccessful():
        return 0
    return 1

if __name__ == '__main__':
    manager.run()
